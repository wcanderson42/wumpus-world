package player;

import gui.Cell;
import gui.CellOwner;
import gui.CellProperty;

public class CellKnowledge {
    private Cell cell; //The cell in question
    private CellProperty cellProperty; //What features this cell has
    private CellOwner cellOwner; //Who might be in the cell (Wumpus, Pit, Player)
    private boolean guess; //Is this knowledge a guess or known

    public CellKnowledge(Cell cell, CellProperty property, CellOwner owner, boolean guess)
    {
        this.cell = cell;
        cellProperty = property;
        cellOwner = owner;
        this.guess = guess;
    }

    public Cell getCell(){return cell;}
    public CellProperty getCellProperty(){return cellProperty;}
    public CellOwner getCellOwner(){return cellOwner;}
    public boolean isGuess(){return guess;}

    public boolean hasCell(Cell cell)
    {
        return this.cell.equals(cell);
    }

}
