package gui;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by coltenanderson on 10/9/17.
 */
public class WumpusFileReader {
    private static WumpusFileReader ourInstance = new WumpusFileReader();

    public static WumpusFileReader getInstance() {
        return ourInstance;
    }


    private WumpusFileReader() {
    }

    public void insertPoints(WumpusField field, String filename)
    {
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while(line != null)
            {
                char type_code = Character.toLowerCase(line.charAt(0));
                int x = Integer.parseInt(line.substring(2,2));
                int y = Integer.parseInt(line.substring(4,4));
                if (type_code == 'p') //gui.PitCell
                {
                    field.addSpecialCell(x,y,new PitCell(field));
                }
                else if (type_code == 'w') //gui.WumpusCell
                {
                    field.addSpecialCell(x,y,new WumpusCell(field));
                }
                else if (type_code == 'g') //gui.GoldCell
                {
                    field.addSpecialCell(x,y,new GoldCell(field));
                }
                else if (type_code == 'b') //gui.WallCell
                {
                    field.addSpecialCell(x,y,new WallCell(field));
                }
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
