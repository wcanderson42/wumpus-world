package gui;

/**
 * Created by coltenanderson on 10/9/17.
 */
public class WallCell extends Cell {

    public WallCell(WumpusField container) {
        super(container);
        canEnter = false;
    }

    @Override
    public void fireInto() {
        //nothing happens
    }

    @Override
    public boolean moveInto() {
        //shouldnt be able to enter here
        return false;
    }

    @Override
    public boolean grab() {
        //should not be in the cell so should not be able to grab
        return false;
    }
}
