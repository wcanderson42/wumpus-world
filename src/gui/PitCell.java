package gui;

/**
 * Created by coltenanderson on 10/5/17.
 */
public class PitCell extends Cell {

    public PitCell(WumpusField container) {
        super(container);
        adjustProperties(new boolean[]{false, true, false});
    }

    @Override
    public void fireInto() {
        //nothing happens
    }

    @Override
    public boolean moveInto() {
        // player falls to their death
        return true;
    }

    @Override
    public boolean[] getProperties()
    {
        return new boolean[]{false, true, false};
        // returns base properties so that adjacent special cells do not carry properties more than 1 cell
    }

    @Override
    public boolean grab() {
        return false; // no gold here
    }
}
