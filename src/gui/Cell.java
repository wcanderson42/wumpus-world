package gui;

/**
 * Created by coltenanderson on 10/4/17.
 */
public abstract class Cell {
    protected WumpusField driver;
    protected char[] picture;
    protected boolean[] properties = {false, false, false}; //smell, breeze, glimmer
    protected boolean isExit = false; //is the agent allowed to leave from this cell
    protected boolean canEnter = true; //can the agent move into this cell

    public Cell(WumpusField container)
    {
        this.driver = container;
    }

    public boolean[] getProperties()
    {
        return properties;
    }

    public void adjustProperties(boolean[] props)
    {
        for(int i = 0; i< props.length; i++)
        {
            if(props[i])
                properties[i] = true;
        }
    }

    public boolean isExit() //return true if the player can exit from this tile
    {
        return isExit;
    }

    public boolean canEnter()
    {
        return canEnter;
    }
    public abstract void fireInto(); // shoot arrow
    public abstract boolean moveInto(); // move agent into, returns true if player dies
    public abstract boolean grab();  // reach for gold


}
