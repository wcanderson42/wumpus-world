package gui;

import java.awt.*;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Created by coltenanderson on 10/5/17.
 */
public class WumpusField {

    Cell[][] field = new Cell[10][10];
    ArrayList<Point> special_cell_locations = new ArrayList<Point>();

    public WumpusField() {
        for (Cell[] row : field) {
            row = Stream.generate(() -> new EmptyCell(this))
                    .limit(5)
                    .toArray(EmptyCell[]::new);
        }
    }

    void distributeProps() {
        // for each non empty cell distribute its properties to the cells next to it.
        for (Point p : special_cell_locations) {
            int x = (int) p.getX();
            int y = (int) p.getY();

            boolean[] props = field[x][y].getProperties();

            if (x < 9) // there is a cell to the left
                field[x + 1][y].adjustProperties(props);

            if (x > 0) // there is a cell to the right
                field[x - 1][y].adjustProperties(props);

            if (y < 9) // there is a cell above
                field[x][y + 1].adjustProperties(props);

            if (y > 0) // there is a cell below
                field[x][y - 1].adjustProperties(props);
        }


    }

    public void addSpecialCell(int x, int y, Cell new_cell)
    {
        field[x][y] = new_cell;
        special_cell_locations.add(new Point(x,y));
    }
}
