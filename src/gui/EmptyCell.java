package gui;

/**
 * Created by coltenanderson on 10/4/17.
 */
public class EmptyCell extends Cell {

    public EmptyCell(WumpusField container) {
        super(container);
    }

    @Override
    public void fireInto() {
        // nothing happens
    }

    @Override
    public boolean moveInto() {
        //nothing is here, player survives
        return false;
    }


    @Override
    public boolean grab() {
        return false; // no gold here
    }
}
