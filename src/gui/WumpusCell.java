package gui;

/**
 * Created by coltenanderson on 10/5/17.
 */
public class WumpusCell extends Cell {
    public WumpusCell(WumpusField container) {
        super(container);
    }

    @Override
    public void fireInto() {
        //kills the Wumpus
    }

    @Override
    public boolean moveInto() {
        //player is killed by the wumpus
        return true;
    }

    @Override
    public boolean grab() {
        return false;
    }

    @Override
    public boolean[] getProperties() {
        return new boolean[]{true, false, false};
        // returns base properties so that adjacent special cells do not carry properties more than 1 cell

    }
}
