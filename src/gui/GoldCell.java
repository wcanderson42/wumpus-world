package gui;

/**
 * Created by coltenanderson on 10/4/17.
 */
public class GoldCell extends Cell {

    public GoldCell(WumpusField container) {
        super(container);
        adjustProperties(new boolean[]{false, false, true});
    }

    public void fireInto() {
        //nothing happens, no bad dude here
    }

    public boolean moveInto() {
        // nothing dangerous here
        return false;
    }

    public boolean grab() {
        return true;
    } // get the gold

    @Override
    public boolean[] getProperties() {
        return new boolean[]{false, false, true};
        // returns base properties so that adjacent special cells do not carry properties more than 1 cell

    }

}
